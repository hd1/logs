import cStringIO as StringIO
import csv
import datetime
import hashlib
import json
import logging
import os
from xml.dom.minidom import Document

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import psycopg2
import web

import test

# FIXME add IPN and associated tables

urls = (
    r'/', 'Main',
    r'/data(.*)', 'Out',
    r'/hash', 'Hash',
)

app = web.application(urls, globals())

HOME_IP = '76.91.241.220'
HOST = 'ec2-54-235-147-211.compute-1.amazonaws.com'
USER = 'zlkmmlvyqrvdfx'
PASSWORD = 'XzqZa3_O7v87UESbv-S82lt6FU'
DB_NAME = 'df2dg6o94unv2v'


def log(metadata_, application):
    conn = psycopg2.connect(user=USER,
                            host=HOST,
                            database=DB_NAME,
                            password=PASSWORD,
                            sslmode='require')
    metadata = json.dumps(metadata_)
    c = conn.cursor()
    c.execute("INSERT INTO applicationlogs(app_id, metadata) VALUES (upper(encode(digest(%s, 'sha1'),'hex')),%s)", (application, metadata))
    conn.commit()
    c.close()


class Hash:
    def POST(self):
        string = web.input()['q']
        ret = {'hash': {'sha1': hashlib.sha1(string).hexdigest().upper(),
                        'sha224': hashlib.sha224(string).hexdigest().upper(),
                        'md5': hashlib.md5(string).hexdigest().upper(),
                        'sha512': hashlib.sha512(string).hexdigest().upper()
                    }
           }
        return json.dumps(ret)

    def GET(self):
        return self.POST()


class Out:
    def GET(self, data_format):
        form = data_format[1:]
        conn = psycopg2.connect(user=USER, host=HOST, database=DB_NAME, password=PASSWORD, sslmode='require')
        c = conn.cursor()
        incoming_param = web.input()

        try:
            data_to_log = {'app_id': 'logging', 'endpoint': 'GET /data.{0}?app_id='.format(form, incoming_param['app_id']), 'remote': incoming_param['remote']}
        except KeyError:
            data_to_log = {'remote': test.get_my_ip(), 'endpoint': 'GET /data.{0}'.format(form)}
            c = conn.cursor()
            c.execute("SELECT app_id from applicationlogs group by app_id order by count(app_id) limit 1")
            results = c.fetchone()
            incoming_param['app_id'] = results[0]

        c.execute("SELECT upper(encode(digest(app_id, 'sha1'), 'hex')) as app_id, (extract(epoch from read_on::timestamptz)*1000)::text as read_on, metadata from applicationlogs")
        results = c.fetchall()
        c.close()
        logging.debug(incoming_param)
        log(data_to_log, "logging")

        results = [r for r in results]
        header = ['Application', 'Timestamp', 'Metadata']
        frame = pd.DataFrame(results, columns=header)
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        if form == 'json':
            web.header('Content-Type', 'text/plain')
            returnVal_raw = frame.to_json(force_ascii=False, orient='split')
            returnVal = json.loads(returnVal_raw)
            return json.dumps(returnVal['data'])

        elif form == 'latex' or form == 'tex':
            web.header('Content-Type', 'text/plain')
            return r'''\document{article}\begin{document}'''+frame.to_latex()+r'\end{article}\end{document}'

        elif form == 'html':
            web.header('Content-Type', 'text/html')
            raise web.seeother('/static/sample.html')

        elif form == 'excel' or form == 'xls' or form == 'xlsx':
            frame.to_excel('{0}.xls'.format(incoming_param['app_id']), 'Logging data', na_rep='not supplied')
            with open('{0}.xls'.format(incoming_param['app_id'])) as fin:
                web.header('Content-Type', 'application/vnd.ms-excel')
                return fin.read()
            os.unlink('{0}.xls')

        # FIXME stata file output format
        # elif form == 'stata':
        #     return frame.to_stata()

        elif form == 'xml':
            web.header('Content-Type', 'text/xml')
            return self.to_xml(frame)

        elif form == 'csv':
            web.header('Content-Type', 'text/plain')
            return frame.to_csv(index=False, encoding='utf-8', float_format='%f', escapechar='\\', doublequote=False)

        raise web.HTTPError(404, 'Must specify a valid format for data')

    def to_xml(self, df):
        doc = Document()
        log = doc.createElement('Log')
        doc.appendChild(log)
        items = doc.createElement('items')

        for index, row in df.iterrows():
            element = doc.createElement('item')
            element.setAttribute('application', row['Application'])
            element.setAttribute('date', str(row['Timestamp']))
            element.setAttribute('metadata', row['Metadata'])
            items.appendChild(element)

            log.appendChild(items)
            out = StringIO.StringIO()
            doc.writexml(out, indent='', addindent='', newl='')

        return out.getvalue()


class Main:
    def POST(self):
        """
        Two parameters:
        - app_id is the unhashed application name
        - metdata what data you want to store with the database hit
        """
        incoming_params = web.input()

        # TODO add validation for app_id in database
        # if not incoming_params['app_id'] in self.approved:
        #     raise web.HTTPError(422)
        local_ip = test.get_my_ip()
        logging.debug(local_ip)

        data_to_log = {'app_id': incoming_params['app_id'], 'metadata': json.dumps({'metadata': '{0}'.format(incoming_params['metadata'])}), 'remote': local_ip}
        log(data_to_log, 'logging')
        web.header('X-Application-Id', incoming_params['app_id'])
        return json.dumps({"Application-Id": '{0}'.format(incoming_params['app_id'])})

    def GET(self):
        out = Out()
        data_in_raw = out.GET('.json')
        logging.debug(data_in_raw)
        data_in = json.loads(data_in_raw)
        logging.debug('{0} should be a timestamp'.format(data_in[0][1]))
        data = pd.DataFrame(data_in)
        hours = []
        for d in data[1]:
            incoming_date = d[:d.rfind('.')]
            logging.debug('timestamp stripped: {0}'.format(incoming_date))
            stamp_as_int = int(incoming_date)
            logging.debug('timestamp as int: {0}'.format(stamp_as_int))
            stamp_for_datetime = stamp_as_int/1000
            logging.debug("Python's notion of epoch time: {0}".format(stamp_for_datetime))
            stamp_as_datetime = datetime.datetime.fromtimestamp(stamp_for_datetime)
            logging.debug('datetime: {0}'.format(stamp_as_datetime))
            hour_as_string = stamp_as_datetime.strftime('%H')
            hour = int(hour_as_string)
            logging.debug('Hour of day: {0}'.format(hour))
            hours.append(hour)
        logging.debug(hours)
        hours_as_array = np.asarray(hours)

        plt.figure()
        logging.debug('matplotlib initialised!')
        plt.hist(hours_as_array, bins=range(0, 24))
        logging.debug('histogram set up!')
        plt.suptitle('Hourly Hits on logging.d8u.us')
        plt.title('UTC/Zulu')
        logging.debug('title set!')
        plt.xlabel('Hour of Day')
        logging.debug('x-axis set!')
        plt.ylabel('Number of hits')
        logging.debug('y-axis set!')
        svg = StringIO.StringIO()
        logging.debug('SVG initialised!')
        plt.savefig(svg, format='svg')
        logging.debug('svg saved!')
        svg = svg.getvalue()
        logging.debug(svg)
        web.header('Content-Type', "image/svg+xml")
        return svg


if __name__ == '__main__':
    logging.basicConfig(level=logging.FATAL)

    conn = psycopg2.connect(user=USER, host=HOST, database=DB_NAME, password=PASSWORD, sslmode='require')
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS applicationlogs (id serial primary key, read_on timestamp default now(), app_id char(40) not null, metadata text not null, constraint appid_length check (length(app_id) = 40))')
    # TODO: constraint appid_valid check (select app_id from approved_app_ids where expiration_date > curdate() or expiration_date is null)
    # c.execute('CREATE TABLE IF NOT EXISTS approved_app_ids (app_id char(40) primary key, expiration_date date)')
    conn.commit()
    c.close()

    app.run()
