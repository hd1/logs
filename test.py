import argparse
import hashlib
import json
import logging

import requests

args = None


def get():
    response = requests.get('http://{0}/data.json'.format(args.HOST), params = {'app_id':'logging'}).json()
    return response

def get_my_ip():
    ip = requests.get('http://api.ipify.org').text
    return ip

def post_to_logging():
    endpoint = 'http://{0}'.format(args.HOST)
    data = {'application': 'logging', 'remote': get_my_ip(), 'value':'success'}
    params = {'app_id':'logging', 'metadata':json.dumps(data)}
    response = requests.post(endpoint, params=params)
    print response.content
    
def test_post():
    try:
        old_length = len(get())
    except ValueError:
        old_length = 0
    
    post_to_logging()
    print '{0} should be greater than {1}'.format(len(get()), old_length)
    return len(get()) > old_length

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument('HOST', type=str, default='logging.d8u.us', help='hostname:portnumber for app endpoint')
    args = parser.parse_args()
    if not test_post():
        print 'post failed'
    
